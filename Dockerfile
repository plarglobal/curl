FROM debian:8

MAINTAINER Yakov Zubarev "boris.glebov@hotmail.com"

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        ca-certificates \
        curl \
        wget \
    && rm -rf /var/lib/apt/lists/*
